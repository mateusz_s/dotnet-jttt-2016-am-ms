﻿using System.ComponentModel;

namespace JTTT
{
    public class JtttTask
    {
        public virtual JtttCondition Condition { get; set; }
        public virtual JtttAction Action { get; set; }
        public int Id { get; set; }

        public JtttTask(JtttCondition cond, JtttAction act)
        {
            Condition = cond;
            Action = act;
        }

        public JtttTask(JtttTask oldTask)
        {
            Condition = oldTask.Condition;
            Action = oldTask.Action;
        }

        public JtttTask()
        {

        }

        public BindingList<string> ProcessTask()
        {
            return Condition.ProcessTask();
        }

        public bool ExecuteAction()
        {
            var processedTasks = ProcessTask();

            foreach (var task in processedTasks)
            {
            //    Logger.Debug($"Execute action for: {task.ToString()}");
                Action.ExecuteAction(task);
            }
            return true;
        }
        public override string ToString()
        {
            return Condition.ToString() + Action.ToString(); // todo: tostring w condition i action
        }
    }

}
