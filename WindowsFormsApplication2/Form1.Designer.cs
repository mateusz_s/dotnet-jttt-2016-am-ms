﻿namespace JTTT
{
    partial class Ifttt
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.url = new System.Windows.Forms.TextBox();
            this.searchForText = new System.Windows.Forms.TextBox();
            this.emailAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.addToListWeatherButton = new System.Windows.Forms.Button();
            this.taskBox = new System.Windows.Forms.ListBox();
            this.executeButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.sendMail = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.AddToListButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.detailsButton = new System.Windows.Forms.Button();
            this.temperature = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.city = new System.Windows.Forms.TextBox();
            this.sendMail.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // url
            // 
            this.url.BackColor = System.Drawing.Color.Gainsboro;
            this.url.Location = new System.Drawing.Point(68, 38);
            this.url.Name = "url";
            this.url.Size = new System.Drawing.Size(337, 20);
            this.url.TabIndex = 0;
            // 
            // searchForText
            // 
            this.searchForText.BackColor = System.Drawing.Color.Gainsboro;
            this.searchForText.Location = new System.Drawing.Point(69, 79);
            this.searchForText.Name = "searchForText";
            this.searchForText.Size = new System.Drawing.Size(337, 20);
            this.searchForText.TabIndex = 1;
            // 
            // emailAddress
            // 
            this.emailAddress.BackColor = System.Drawing.Color.Gainsboro;
            this.emailAddress.Location = new System.Drawing.Point(77, 41);
            this.emailAddress.Name = "emailAddress";
            this.emailAddress.Size = new System.Drawing.Size(337, 20);
            this.emailAddress.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(25, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "URL";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(18, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tekst";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(21, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "E-mail";
            // 
            // addToListWeatherButton
            // 
            this.addToListWeatherButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.addToListWeatherButton.Location = new System.Drawing.Point(77, 92);
            this.addToListWeatherButton.Name = "addToListWeatherButton";
            this.addToListWeatherButton.Size = new System.Drawing.Size(129, 48);
            this.addToListWeatherButton.TabIndex = 6;
            this.addToListWeatherButton.Text = "Dodaj do listy";
            this.addToListWeatherButton.UseVisualStyleBackColor = true;
            this.addToListWeatherButton.Click += new System.EventHandler(this.addToListWeatherButton_Click);
            // 
            // taskBox
            // 
            this.taskBox.FormattingEnabled = true;
            this.taskBox.HorizontalScrollbar = true;
            this.taskBox.Location = new System.Drawing.Point(632, 33);
            this.taskBox.Name = "taskBox";
            this.taskBox.Size = new System.Drawing.Size(378, 134);
            this.taskBox.TabIndex = 7;
            // 
            // executeButton
            // 
            this.executeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.executeButton.Location = new System.Drawing.Point(632, 189);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(131, 57);
            this.executeButton.TabIndex = 8;
            this.executeButton.Text = "Wykonaj!";
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.AllowDrop = true;
            this.clearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.clearButton.Location = new System.Drawing.Point(769, 189);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(125, 57);
            this.clearButton.TabIndex = 11;
            this.clearButton.Text = "Czyść";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // sendMail
            // 
            this.sendMail.AccessibleDescription = "";
            this.sendMail.AccessibleName = "";
            this.sendMail.Controls.Add(this.tabPage1);
            this.sendMail.Controls.Add(this.tabPage2);
            this.sendMail.Location = new System.Drawing.Point(12, 33);
            this.sendMail.Name = "sendMail";
            this.sendMail.SelectedIndex = 0;
            this.sendMail.Size = new System.Drawing.Size(558, 462);
            this.sendMail.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.AddToListButton);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.url);
            this.tabPage1.Controls.Add(this.searchForText);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(550, 436);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Sprawdz strone";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(13, 214);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "E-mail";
            // 
            // AddToListButton
            // 
            this.AddToListButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddToListButton.Location = new System.Drawing.Point(69, 263);
            this.AddToListButton.Name = "AddToListButton";
            this.AddToListButton.Size = new System.Drawing.Size(129, 48);
            this.AddToListButton.TabIndex = 9;
            this.AddToListButton.Text = "Dodaj do listy";
            this.AddToListButton.UseVisualStyleBackColor = true;
            this.AddToListButton.Click += new System.EventHandler(this.AddToListButton_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox1.Location = new System.Drawing.Point(69, 212);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(337, 20);
            this.textBox1.TabIndex = 7;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl1);
            this.tabPage2.Controls.Add(this.temperature);
            this.tabPage2.Controls.Add(this.Label6);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.city);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(550, 436);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sprawdz pogode";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(29, 181);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(468, 213);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.addToListWeatherButton);
            this.tabPage3.Controls.Add(this.emailAddress);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(460, 187);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Wyslij email";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.detailsButton);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(460, 187);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Wyswietl szczegolowe informacje";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // detailsButton
            // 
            this.detailsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.detailsButton.Location = new System.Drawing.Point(110, 47);
            this.detailsButton.Name = "detailsButton";
            this.detailsButton.Size = new System.Drawing.Size(208, 84);
            this.detailsButton.TabIndex = 7;
            this.detailsButton.Text = "Wyświetl szczegółowe informacje";
            this.detailsButton.UseVisualStyleBackColor = true;
            this.detailsButton.Click += new System.EventHandler(this.detailsButton_Click);
            // 
            // temperature
            // 
            this.temperature.BackColor = System.Drawing.Color.Gainsboro;
            this.temperature.Location = new System.Drawing.Point(134, 87);
            this.temperature.Name = "temperature";
            this.temperature.Size = new System.Drawing.Size(337, 20);
            this.temperature.TabIndex = 4;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(27, 86);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(92, 18);
            this.Label6.TabIndex = 3;
            this.Label6.Text = "Temperatura";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(62, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Miasto:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // city
            // 
            this.city.BackColor = System.Drawing.Color.Gainsboro;
            this.city.Location = new System.Drawing.Point(134, 37);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(337, 20);
            this.city.TabIndex = 1;
            // 
            // Ifttt
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1055, 511);
            this.Controls.Add(this.sendMail);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.executeButton);
            this.Controls.Add(this.taskBox);
            this.Name = "Ifttt";
            this.Text = "IFTTT";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.sendMail.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox url;
        private System.Windows.Forms.TextBox searchForText;
        private System.Windows.Forms.TextBox emailAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button addToListWeatherButton;
        private System.Windows.Forms.ListBox taskBox;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.TabControl sendMail;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox city;
        private System.Windows.Forms.TextBox temperature;
        private System.Windows.Forms.Label Label6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button detailsButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AddToListButton;
        private System.Windows.Forms.TextBox textBox1;
    }
}

