﻿using System;

namespace JTTT
{
    internal class Logger
    {
        private bool _loggerOn;
        private string _filePath = "default.log";
        private string _component = "";
        private static Logger _instanceLogger;
        private System.IO.StreamWriter _logStream;

        private Logger()
        {
            _loggerOn = true;
        }

        public static Logger Instance {
            get
            {
                 return _instanceLogger ?? (_instanceLogger = new Logger());
            }
           
        }

        public void SetComponentName(string componentName)
        {
            _component = componentName;
        }

        public void SetFilePath(string pathToFile)
        {
            _filePath = pathToFile;
        }
        public void Enable()
        {
            _loggerOn = true;
        }
        public void Disable()
        {
            _loggerOn = false;
        }
        private void Print(string text, string level)
        {
            if (!_loggerOn) return;
            if (_logStream == null)
            {
                _logStream = new System.IO.StreamWriter(_filePath, true);
            }
            var textToWrite = GenerateLogLine(text, level);
            _logStream.WriteLine(textToWrite);
            _logStream.Flush();
        }
        public static void Info(string text)
        {
            _instanceLogger.Print(text, "INF");
        }
        public static void Error(string text)
        {
            _instanceLogger.Print(text, "ERR");
        }
        public static void Debug(string text)
        {
            _instanceLogger.Print(text, "DBG");
        }
        private string GenerateLogLine(string text, string level)
        {
            var time = DateTime.Now;
            return time.ToString("yyyy-MM-dd HH:mm:ss.ffffff") + " [" + level + "] " + _component + ": " + text;
        }
    }
}