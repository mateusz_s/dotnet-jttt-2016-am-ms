﻿using System.Net;
using Newtonsoft.Json;

namespace JTTT
{
    public class WeatherDownloader
    {
        private Weather weather;
        public WeatherDownloader()
        {
        }

        public static WeatherRootObject DownloadWeatherInfo(string city)
        {
            var weather = new WeatherRootObject();
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("http://api.openweathermap.org/data/2.5/weather?q=" + city + "&APIKEY=4c93cd4be49187db8ed362bfc6dd3087");
                weather = JsonConvert.DeserializeObject<WeatherRootObject>(json);
            }
            return weather;
        }
    }
}
