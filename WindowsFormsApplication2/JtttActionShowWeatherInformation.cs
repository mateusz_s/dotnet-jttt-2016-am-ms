﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    class JtttActionShowWeatherInformation : JtttAction
    {
        public JtttActionShowWeatherInformation()
        {
        }

        public override string ToString()
        {
            return " pokaż informacje o pogodzie";
        }

        public override bool ExecuteAction(string message)
        {
            var frm = new WeatherForm();
 
            Program.mainForm.BeginInvoke((MethodInvoker)delegate()
            {
                frm.weatherDescription.Text = message;
                frm.Show();
            });

            return true;
        }
    }
}
