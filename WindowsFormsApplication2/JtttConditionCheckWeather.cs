﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace JTTT
{
    class JtttConditionCheckWeather : JtttCondition
    {
        public string City { get; set; }
        public string Temperature { get; set; }

        public JtttConditionCheckWeather()
        {
            
        }

        public JtttConditionCheckWeather(string city, string temp)
        {
            City = city;
            Temperature = temp;
        }

        public override string ToString()
        {
            return "Sprawdz pogode prognozy we " + City;
        }

        public override BindingList<string> ProcessTask()
        {
            BindingList<string> weatherMessages = new BindingList<string>();
            var weather = WeatherDownloader.DownloadWeatherInfo(City);
            var tempInCelsius = weather.main.temp - 273;
            if (tempInCelsius > Convert.ToDouble(Temperature))
            {
                weatherMessages.Add("Temperatura w " + City + " wynosi: " + tempInCelsius + "°C.");
            }

            return weatherMessages;
        }
    }
}
