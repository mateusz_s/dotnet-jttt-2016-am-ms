﻿using System.Linq;
using System.ComponentModel;
using System.Windows.Forms.VisualStyles;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace JTTT
{
    internal class TaskManager
    {

        public void AddTaskToDb(JtttTask task)
        {
            using (var ctx = new JtttDbContext())
            {
                //Logger.Debug($"Adding task to database {task.ToString()}");
                ctx.JtttTasks.Add(task);
                ctx.SaveChanges();
            }
        }

        public void ClearTaskFromDb(JtttTask task)
        {
            using (var ctx = new JtttDbContext())
            {
                var taskToRemove = ctx.JtttTasks.First(x => x.Id == task.Id);

                ctx.JtttTasks.Remove(taskToRemove);
                ctx.SaveChanges();
            }
        }

        public void ClearAllTasksFromDb()
        {
            using (var ctx = new JtttDbContext())
            {
                ctx.JtttTasks.RemoveRange(ctx.JtttTasks);
                ctx.SaveChanges();
            }
        }

        private static bool PerformTask(JtttTask task)
        {
           // Logger.Debug($"Performing task: {task.Id}. {task.Condition} {task.Action}");
            task.ProcessTask();
          //  Logger.Debug($"Execute task: {task.Id}. {task.Condition} {task.Action}");
            task.ExecuteAction();
          //  Logger.Debug($"Performed task: {task.Id}. {task.Condition} {task.Action}");
           
            return true;
        }

        public void PerformAllTasks()
        {
            Logger.Info("Performing all tasks from the list.");
            
            List<Task> taskList = new List<Task>();

            using (var ctx = new JtttDbContext())
            {
                foreach (var task in ctx.JtttTasks)
                {
                    PerformTask(new JtttTask(task));  
                    ClearTaskFromDb(task);
                }
            }

            Task.WaitAll(taskList.ToArray());
            
        }

        public BindingList<JtttTask> GetTaskList()
        {
            var list = new BindingList<JtttTask>();
            using (var ctx = new JtttDbContext())
            {
                foreach (var task in ctx.JtttTasks)
                {
                    //Logger.Debug($"Adding task to list {task.ToString()}");
                    list.Add(new JtttTask(task.Condition, task.Action));
                }
            } 
            return list;
        }
    }
}
