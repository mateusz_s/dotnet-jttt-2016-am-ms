namespace JTTT
{
    public abstract class JtttAction
    {
        public int Id { get; set; }

        public override string ToString()
        {
            return "Default Action";
        }

        public abstract bool ExecuteAction(string message);
    }
}