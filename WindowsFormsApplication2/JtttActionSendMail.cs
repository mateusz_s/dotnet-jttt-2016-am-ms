﻿namespace JTTT
{
    class JtttActionSendMail : JtttAction
    {
        public string EMail { get; set; }

        public JtttActionSendMail()
        {

        }

        public JtttActionSendMail(string mail)
        {
            EMail = mail;
        }

        public override string ToString()
        {
            return " wyslij email na " + EMail;
        }

        public override bool ExecuteAction(string message)
        {
            Logger.Debug("execute sending mail: " + message);
            Mail.SendMail("JTTT", message, EMail);
            return true;
        }
    }
}
