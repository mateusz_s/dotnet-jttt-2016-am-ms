﻿using HtmlAgilityPack;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using JTTT;
using System.ComponentModel;


namespace Html
{
    public class HtmlDownloader
    {
        private readonly string _url;

        public HtmlDownloader(string url)
        {
            _url = url;
        }

        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = WebUtility.HtmlDecode(wc.DownloadString(_url));

                Logger.Info("Html page downloaded from: " + _url);
                return html;
            }
        }

        public BindingList<string> GetMessagesToSend(string tag)
        {
            var doc = new HtmlDocument();
            var pageHtml = GetPageHtml();

            doc.LoadHtml(pageHtml);

            var nodes = doc.DocumentNode.Descendants("img");
            var messages = new BindingList<string>();

            foreach (var node in nodes.Where(node => new Regex(tag).IsMatch(node.GetAttributeValue("alt", ""))))
            {
                var url = node.GetAttributeValue("src", "");
                Logger.Info("Found matching image for tag: " + tag + " url: " + url);
                messages.Add(url);
            }

            return messages;
        }
    }
}