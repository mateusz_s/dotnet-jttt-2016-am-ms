﻿using System;
using System.Windows.Forms;

namespace JTTT
{
    static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        public static Ifttt mainForm;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
           
            Application.SetCompatibleTextRenderingDefault(false);
            mainForm = new Ifttt();
            Application.Run(mainForm);
        }
    }
}
