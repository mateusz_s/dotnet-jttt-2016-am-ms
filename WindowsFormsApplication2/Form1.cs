﻿using System;
using System.Windows.Forms;

namespace JTTT
{
    public partial class Ifttt : Form
    {
        readonly TaskManager _taskManager;
        
        public Ifttt()
        {
            _taskManager = new TaskManager();
            InitializeComponent();
            Logger.Instance.SetComponentName("JTTT");
            taskBox.DataSource = _taskManager.GetTaskList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            Logger.Error("I DONT WANT TO CLICK IT EVER");
        }

        private void addToListWeatherButton_Click(object sender, EventArgs e)
        {
            Logger.Info("Add to list weather button clicked.");
            var text = searchForText.Text;
            _taskManager.AddTaskToDb(new JtttTask(new JtttConditionCheckWeather(city.Text, temperature.Text), new JtttActionSendMail(emailAddress.Text)));
            taskBox.DataSource = _taskManager.GetTaskList();
        }
        
        private void executeButton_Click(object sender, EventArgs e)
        {
            Logger.Info("Execute button clicked.");
            _taskManager.PerformAllTasks();
            taskBox.DataSource = _taskManager.GetTaskList();

        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            Logger.Info("Clear button clicked.");
            _taskManager.ClearAllTasksFromDb();
            taskBox.DataSource = _taskManager.GetTaskList();

        }

        private void taskBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void AddToListButton_Click_1(object sender, EventArgs e)
        {
            Logger.Info("Add to list button clicked.");
            var text = searchForText.Text;
            _taskManager.AddTaskToDb(new JtttTask(new JtttCondOneWordInDesc(text, url.Text), new JtttActionSendMail(textBox1.Text)));
            taskBox.DataSource = _taskManager.GetTaskList();
        }

        private void detailsButton_Click(object sender, EventArgs e)
        {
            _taskManager.AddTaskToDb(new JtttTask(new JtttConditionCheckWeather(city.Text, temperature.Text), new JtttActionShowWeatherInformation()));
            taskBox.DataSource = _taskManager.GetTaskList();
        }
    }
}
