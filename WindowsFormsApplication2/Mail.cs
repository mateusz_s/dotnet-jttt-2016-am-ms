﻿using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
//using System.Text.Encoding;

namespace JTTT
{
    public class Mail
    {
        private static Mail _mailInstance;
        private Mail() {  }

        public static Mail Instance
        {
            get
            {
                return _mailInstance ?? (_mailInstance = new Mail());
            }
        }
        

        public static void SendMail(string topic, string message, string email)
        {
            Logger.Info("Sendind mail with topic: " + topic + " to: " + email);
            var client = new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("jtttmsam@gmail.com", "jtttmsam1")
            };

            var mm = new MailMessage("donotreply@domain.com", email, topic, message)
            {
                //BodyEncoding = UTF8,
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            };

            client.Send(mm);
        }
    }
}