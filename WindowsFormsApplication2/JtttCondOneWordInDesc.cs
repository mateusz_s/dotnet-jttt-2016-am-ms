﻿using System.ComponentModel;

namespace JTTT
{
    class JtttCondOneWordInDesc : JtttCondition
    {
        public string RegExpToSearch { get; set; }
        public string Url { get; set; }

        public JtttCondOneWordInDesc()
        {
            
        }

        public JtttCondOneWordInDesc(string regex, string url)
        {
            RegExpToSearch = regex;
            Url = url;
        }

        public override string ToString()
        {
            return "Jesli opis ze strony " + Url + " zawiera slowo " + RegExpToSearch;
        }

        public override BindingList<string> ProcessTask()
        {
            Html.HtmlDownloader htmlDownloader = new Html.HtmlDownloader(Url);

            var htmlPage = htmlDownloader.GetPageHtml();

            var messageList = htmlDownloader.GetMessagesToSend(RegExpToSearch);

            foreach (var message in messageList)
            {
                Logger.Debug("Found and prepared message: " + message);
            }

            return messageList;
        }
    }
}
