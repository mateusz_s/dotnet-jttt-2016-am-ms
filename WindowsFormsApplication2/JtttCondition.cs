using System.ComponentModel;

namespace JTTT
{
    public abstract class JtttCondition
    {
        public int Id { get; set; }

        public override string ToString()
        {
            return "Domyslny warunek";
        }

        public abstract BindingList<string> ProcessTask();
    }
}